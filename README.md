# GAMI_Python_WS

This is a nice little introduction to Python written by @author Sohpie Wu (srw62), with some assitance by Kaden Bock (kdb77) for the GAMI Python Skills Workshop.

## How to Start

The best way to start is by "forking" the repository. Click the "forks" button in the top left. Then, add the project to your namespace by selecting your netID from the "select namespace" box.

You can also change the repo to be private at this point.

Then, open the python notebook.
